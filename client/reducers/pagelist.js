const pageLists = (state = [], action) => {
  switch (action.type) {
    case 'ADD_PAGE_LIST':
      return [
        ...state,
        {
          id: action.id,
        }
      ];
    case 'NEXT_PAGE':
      console.log(action.id);
      console.log(action.nextPageId);
      return state.map((pageLists, index) => {
        console.log(index);
        if (index === action.id) {
          console.log("Next page succes")
          return Object.assign({}, pageLists, {
            currentpage: action.nextPageId
          })
        }
        return pageLists
      })
    default:
      return state
  }
}

export default pageLists
