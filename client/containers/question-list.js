import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Question from '../components/Question.jsx';
import {addQuestion} from '../actions/addQuestion.js'
import RaisedButton from 'material-ui/RaisedButton';



class QuestionList extends Component {
  constructor(props){
    super(props);
    this.state = {
    };
    this.eachQuestion = this.eachQuestion.bind(this);
  }

  eachQuestion(newText, i){
    return(
      <Question key={i} index={i}>
        {newText.title}
        {newText.desc}
        {this.props.children[0]}

      </Question>);
  }

    renderList() {
        return (
                <li>
                  {this.props.questions.map(this.eachQuestion)}
                </li>
        );
      }

    render() {
        return (
          <div>
            <ul>
                {this.renderList()}
            </ul>
        </div>
        );
    }

}

// Get apps state and pass it as props to QuestionList
//      > whenever state changes, the QuestionList will automatically re-render
function mapStateToProps(state) {
    return {
        questions: state.questions
    };
}

// Get actions and pass them as props to to QuestionList
//      > now QuestionList has this.props.selectQuestion
function matchDispatchToProps(dispatch){
    return bindActionCreators({addQuestion: addQuestion}, dispatch);
}

// We don't want to return the plain QuestionList (component) anymore, we want to return the smart Container
//      > QuestionList is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(QuestionList);
