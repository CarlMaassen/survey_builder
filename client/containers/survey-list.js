import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Survey from '../components/Survey.jsx';
import {addSurvey} from '../actions/addSurvey.js'
import RaisedButton from 'material-ui/RaisedButton';

class SurveyList extends Component {
  eachSurvey(newText, i){
    return(
      <Survey key={i} index={i}>
        {newText.title}
        {newText.desc}

      </Survey>);
  }
    
    renderList() {
        return (
                <li>
                  {this.props.surveys.map(this.eachSurvey)}
                </li>
        );
      }

    render() {
        return (
          <div id="newSurveyButton">
            <RaisedButton label="Add new Survey" onClick={() => this.props.addSurvey("Titel Survey", "Survey beschrijving")}></RaisedButton>
            <ul>
                {this.renderList()}
            </ul>
          </div>
        );
    }

}

// Get apps state and pass it as props to SurveyList
//      > whenever state changes, the SurveyList will automatically re-render
function mapStateToProps(state) {
    return {
        surveys: state.surveys
    };
}

// Get actions and pass them as props to to SurveyList
//      > now SurveyList has this.props.selectSurvey
function matchDispatchToProps(dispatch){
    return bindActionCreators({addSurvey: addSurvey}, dispatch);
}

// We don't want to return the plain SurveyList (component) anymore, we want to return the smart Container
//      > SurveyList is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(SurveyList);
