import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Page from '../components/Page.jsx';
import {addPage} from '../actions/addPage.js'
import {nextPage} from '../actions/nextPage.js'
import RaisedButton from 'material-ui/RaisedButton';
import _ from 'lodash';

class PageList extends Component {
  constructor(props){
    super(props);
    this.state = {
      currentpage : 0,
    };
  }
  
  renderList(currentpage) {
    if(this.props.item){
          console.log(this.props.item.index);
      return (
        <div>
          <RaisedButton label="Next Page" onClick={()=>this.props.nextPage(currentpage)}></RaisedButton>
          <Page key={currentpage} index={currentpage} id={currentpage}>
            {this.props.item.title}
            {this.props.item.desc}
          </Page>
        </div>
      );
    }else{
      return(
        <p>No Pages</p>
      )
    }
  }

    render() {
        return (
          <div>
            <RaisedButton label="Add new Page" onClick={() => this.props.addPage("Titel Page", "Page Beschrijving")}></RaisedButton>
            <ul>
            </ul>
          </div>
        );
    }
}

// Get apps state and pass it as props to PageList
//      > whenever state changes, the PageList will automatically re-render
function mapStateToProps(state, ownProps) {
    return {
        pageList: state.pageList,
        item: _.find(state.pages, 'id', ownProps)
    };
}

// Get actions and pass them as props to to PageList
//      > now PageList has this.props.selectPage
function matchDispatchToProps(dispatch){
    return bindActionCreators({addPage: addPage, nextPage: nextPage}, dispatch);
}

// We don't want to return the plain PageList (component) anymore, we want to return the smart Container
//      > PageList is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(PageList);
