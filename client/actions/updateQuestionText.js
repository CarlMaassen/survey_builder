export const updateQuestionText = (newTitle, newDesc, id) => ({
  type: 'UPDATE_QUESTION',
  title: newTitle,
  desc: newDesc,
  id: id,
})
