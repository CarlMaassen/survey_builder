let nextTodoId = 0
export const addPage = (title, desc) => ({
  type: 'ADD_PAGE',
  id: nextTodoId++,
  title: title,
  desc: desc,
  index: nextTodoId,
  questionList: nextTodoId,
})
