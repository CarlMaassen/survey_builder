let nextTodoId = 0
export const addQuestion = (title, desc, page) => ({
  type: 'ADD_QUESTION',
  id: nextTodoId++,
  title: title,
  desc: desc,
  page: page,
})

export const addMultipleChoiceQuestion = (title, desc, page) => ({
  type: 'MULTIPLE_CHOICE_QUESTION',
  id: nextTodoId++,
  question: title,
  desc: desc,
  page: page
})

export const addOpenQuestion = (title, desc, page) => ({
  type: 'OPEN_QUESTION',
  id: nextTodoId++,
  question: title,
  answer: '',
  desc: desc,
  page: page
})

export const addRangeQuestion = (title, desc, page) => ({
  type: 'RANGE_QUESTION',
  id: nextTodoId++,
  question: title,
  desc: desc,
  page: page
})
