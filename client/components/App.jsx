import Survey from './Survey.jsx';
import React from 'react';
import ReactDOM from 'react-dom';
import PageList from '../containers/page-list.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {deepOrange500} from 'material-ui/styles/colors';
import AppBar from 'material-ui/AppBar';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import QuestionList from '../containers/question-list.js';
import SurveyList from '../containers/survey-list.js';

const styles = {
  container: {
    textAlign: 'center',
    paddingTop: 200,
  },
};

//const muiTheme = getMuiTheme(lightBaseTheme);

export default class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <AppBar
            title="Enquete"
            iconClassNameRight="muidocs-icon-navigation-expand-more"
          />
            <SurveyList />
        </div>
      </MuiThemeProvider>
  );
  }
}
