import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import allReducers from './reducers/index.js';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';
import App from './components/App.jsx';

const logger = createLogger();const store = createStore(allReducers,    applyMiddleware(thunk, promise, logger),      window.devToolsExtension ? window.devToolsExtension() : f => f    );

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>
  ,document.getElementById('root'))
